// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Called when the user clicks on the browser action.
/*
chrome.browserAction.onClicked.addListener(function(tab) {
  // No tabs or host permissions needed!
  console.log('Turning ' + tab.url + ' red!');
  chrome.tabs.executeScript({
    code: 'document.body.style.backgroundColor="red"'
  });
});
*/
if($('#d7-signin').length) {
  console.log('test');
  $('#d7-signin a.ga').click();
}

function isInt(n) {
   return typeof n === 'number' && parseFloat(n) == parseInt(n, 10) && !isNaN(n);
} // 6 characters


// move people on the team summaries who are not typically resourced to the bottom of the page
var hideTeams = ["Accounting", "Account Manager", "Administrator", "Business Development", "Content"];
$('#team_summary .team').each(function(){
  if($.inArray($('th:first-child', this).html().trim(), hideTeams) > -1) { 
    console.log('yes');
    $(this).nextUntil('.team').andSelf().appendTo($(this).parent()); 
    $(this).data('hidden', 'true');
  }
});
$('#team_summary .team').click(function(){
  console.log('test');
  if($(this).data('hidden') == 'true') {
    $(this).nextUntil('.team').show();
    $(this).data('hidden', 'false');
  } else {
    $(this).nextUntil('.team').hide();
    $(this).data('hidden', 'true');
  }
});


// Add sum functionality so that values can quickly be added together
$('#team_summaries td, #project_summaries td').click(function(){
  if($(this).hasClass('summed')) {
    $(this).removeClass('summed');
  } else {
    $(this).addClass('summed');
  }
  
  var sum = 0;
  $('.summed').each(function(){
    sum = sum + parseInt($(this).html());
  });

  if($('.sum-counter').length){
    $('.sum-counter').html(sum);
  } else {
    $('body').append('<div class="sum-counter">' + sum + '</div>');
  }
});


// Add summary and heat map functionality to reports page
$('#team_summaries #page-header .actions').prepend('<a class="radius button next-week-report" href="#">NW report</a><a class="radius button harvest-report" href="#">Harvest report</a>');

$('.summary').click(function(){
  if($(this).hasClass('current')) {
    $(this).removeClass('current');
    $('#team_summary tr').not('.person, .team').show();
  } else {
    $(this).addClass('current');
    $('#team_summary tr').not('.person, .team').hide();
  }
});

// Add next week report to reports page
$('.next-week-report').click(function(){
  var overbooked = Array();
  var underbooked = Array();
  var vacation = Array();
  
  $('#page-header').after('<div id="nw-report"><h1>Trend Report</td><div class="nw-content"></div></div>');
  
  $('.team').each(function(){
    var team_id = $(this).attr('id');
    $(this).nextUntil('.team').addClass(team_id);
  });

  $('.team_6.person, .person.team_7, .person.team_3, .person.team_11').each(function(key, val){
    var $person = $(this);
    var hours = $person.find('td:eq(1)').html();
    
    if(hours > 40) {
      overbooked.push($person);
    }
    if(hours < 30) {
      underbooked.push($person);
    }
  });
  
  $('.team_6#project_1, .team_7#project_1, .team_3#project_1, .team_11#project_1').each(function(key, val){
    if(parseInt($(this).find('td:eq(1)').html()) > 0) {
      var $person = $(this).prevAll('.person:first');
      var hours = $(this).find('td:eq(1)').html();
      var name = $person.find('th .row-title img').attr('alt');
      
      vacation.push([name, hours]);
    }
  });
  
  
  var over_content = '<p><strong>Overbooked</strong><br />';
  $.each(overbooked, function(){
    var name = $(this).find('th .row-title img').attr('alt');
    var hours = $(this).find('td:eq(1)').html();
    
    if(parseInt(hours) > 45) {
      over_content += '<span style="color: red;">' + name + ": " + hours + ' hours</span><br />';
    } else {
      over_content += name + ": " + hours + ' hours<br />';
    }
  });
  over_content += '</p>';
  $('.nw-content').append(over_content);
  
  var under_content = '<p><strong>Underbooked</strong><br />';
  $.each(underbooked, function(){
    var name = $(this).find('th .row-title img').attr('alt');
    var hours = $(this).find('td:eq(1)').html();
    under_content += name + ": " + hours + ' hours<br />';
  })
  under_content += '</p>';
  $('.nw-content').append(under_content);
  
  var vacation_content = '<p><strong>Vacation</strong><br />';
  $.each(vacation, function(){
    vacation_content += $(this)[0] + ": " + $(this)[1] + ' hours<br />';
  })
  vacation_content += '</p>';
  $('.nw-content').append(vacation_content);
});


// Resolve issue where project summaries don't add properly
$('#project_summaries .project').each(function(){
  var sumHours = new Array();
  $(this).nextUntil('.project').each(function(){
    if($(this).hasClass('person')) {
      var hours = new Array();
      $(this).find('td').each(function(key, value){
        hours = parseInt($(this).html());
        if(isInt(hours)) {
          if(sumHours[key] > 0) {
            sumHours[key] = parseInt(sumHours[key]) + hours;
          } else {
            sumHours[key] = hours;
          }
        }
      })
    }
  });
  $(this).find('td').each(function(key, value){
    if(sumHours[key] > 0) {
      $(this).html(sumHours[key]);
    } else {
      $(this).html(0);
    }
  })
});

// Schedule page fill function
function remainingHours($input, resource_id, project_id, start_on) {
  var ajaxUrl = 'https://huddle.domain7.com/schedules.json?';
  
  $.getJSON( ajaxUrl, {
    show: "all",
    start_on: start_on,
    range: 1
  })
    .done(function( data ) {
      var capacity = data[project_id][resource_id][start_on][0]['weekly_hours'];
      var booked = parseInt(data[project_id][resource_id][start_on][0]['resource_total']);
      
      var remaining = 0;
      if(booked < capacity) {
        var remaining = capacity - booked;
      }

      if(remaining > 0) {
        $($input).val(remaining);
      }
    });
}

$('#schedule .weeks-table input').each(function(){
  $('input.selected').off('keypress');
  $('input.selected').removeClass('selected');
  
  $(this).focus(function(){
    $(this).addClass('selected');
    $(this).on('keypress', function(event) {
      if(event.keyCode == 102) {
        event.preventDefault();
        remainingHours($(this), $(this).data('resource_id'), $(this).data('project_id'), $(this).data('start_week_on'));
      }
    });
  });
});


$('.harvest-report').click(function(){
  // Huddle vs. Harvest comparison
  var resource_bookings = new Object;
  $('#team_summary .person').each(function(){
    var personID = $(this).attr('id');
    var personName = $(this).find('th img').attr('alt');
    var personClass = personID + "-child";
    resource_bookings[personName] = new Object;
    
    var personBookings = new Object;
    $('.' + personClass).each(function(key, value){
      if(parseInt($(this).find('td:eq(0)').text()) > 0) {
        personBookings[key] = new Object;
        personBookings[key].projectClient = $(this).find('th strong').text().trim();
        $(this).find('th strong').remove();
        personBookings[key].projectName = $(this).find('th').text().trim();
        personBookings[key].projectHours = parseInt($(this).find('td:eq(0)').text().trim());
      }
      //resource_bookings[personID]['name'] = personName;
    });
    resource_bookings[personName].huddle = personBookings; 
    resource_bookings[personName].harvest = new Object;  
  });
  
  //var harvestURL = "https://domain7.harvestapp.com/exports/243528";
  var harvestURL = prompt("Please enter the Harvest report URL", "https://domain7.harvestapp.com/exports/243528");
  $.ajax({
    url:harvestURL,
    type:'get',
    success:function(data){
      var harvest_dump = $.csv.toObjects(data);
      $.each(harvest_dump, function(key, value){
        //console.log(value["First Name"]);
        resourceName = value["First Name"] + " " + value["Last Name"];
        hoursClient = value["Client"];
        hoursProject = value["Project"];
        hoursRef = hoursClient + " " + hoursProject;
        if(typeof(resource_bookings[resourceName]) !== "undefined") {
          if(typeof(resource_bookings[resourceName].harvest[hoursRef]) !== "undefined") {
            resource_bookings[resourceName].harvest[hoursRef] += parseFloat(value["Hours"]);
          } else {
            resource_bookings[resourceName].harvest[hoursRef] = parseFloat(value["Hours"]);
          }
        }
      });
      
      
      $('#page-header').after('<div id="harvest-report"><h1>Harvest Report</td><div class="harvest-content"></div></div>');
      var html = "<table>";
      $.each(resource_bookings, function(key, value){
        html += "<tr>"
          html += "<th colspan='2'>" + key + "</th>";
        html += "</tr><tr>";
          html += "<td><table>";
          html += "<tr>";
            html+= '<td colspan="2">Huddle Booking</td>';
          html += "</tr>"
            $.each(value.huddle, function(key, value){
              html += "<tr>";
                html += '<td>' + value.projectClient + ": " + value.projectName + "</td><td>" + value.projectHours + "</td>";
              html += "</tr>";
            });
          html += "</td></table>";
          html += "<td><table>";
          html += "<tr>";
            html+= '<td colspan="2">Harvest Booking</td>';
          html += "</tr>"
            $.each(value.harvest, function(key, value){
              html += "<tr>";
                html += '<td>' + key + "</td><td>" + value + "</td>";
              html += "</tr>";
            });
          html += "</td></table>";
        
  
          
      });
      html += "</table>";
      $('.harvest-content').append(html);
      
      
    }
  })
});




































